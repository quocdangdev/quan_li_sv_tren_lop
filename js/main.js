var dsSv=[];
var dataJson=localStorage.getItem("DSSV");// LẤY lên
if(dataJson){
   var raw=JSON.parse(dataJson);// convert lại từ Json sang array
   var result=[];
for(var i=0;i<raw.length;i++){
    var dataHienTai=raw[i];
    var sv=new SinhVien(dataHienTai.ma,dataHienTai.ten,dataHienTai.email,dataHienTai.matKhau,dataHienTai.diemToan,dataHienTai.diemLy,dataHienTai.diemHoa)
    result.push(sv);
}
dsSv=result;
randerDSSV(dsSv);
}


function themSv() {


   var sv= layThongTinTuform();
   var isvalid=true;

 isvalid=kiemTraRong(sv.ma,"spanMaSV")& kiemTraRong(sv.ten,"spanTenSV") & kiemTraRong(sv.email,"spanEmailSV") & kiemTraRong(sv.matKhau,"spanMatKhau")

   if(isvalid){
    dsSv.push(sv);
    luuLocalStorage()
    randerDSSV(dsSv);
    resetForm()
   }
   
}
function luuLocalStorage(){
    var dsSvJson=JSON.stringify(dsSv) // convert mảng data thành json để lưu xuống local
    localStorage.setItem("DSSV",dsSvJson); // lưu xuống local
}
function xoaSv(idSv){

    var index=dsSv.findIndex(function(sv){
        return sv.ma==idSv;
    })
    
    if(index==-1){
        return;
    }
    dsSv.splice(index,1);
    randerDSSV(dsSv); 
   // update dữ liệu ssau khi xóa thành ccoong
   luuLocalStorage();

}
function suaSv(idSv){
var index=dsSv.findIndex(function(sv){
return sv.ma==idSv;

})
if(index==-1) return ;

var sv=dsSv[index]
showThongTinLenForm(sv)
//ko cho user thay đổi mã sv khi sữa
document.getElementById("txtMaSV").disabled=true;
}
function capNhapSv(){
    var capNhapSv=layThongTinTuform();
    var index=dsSv.findIndex(function(sv){
        return sv.ma==capNhapSv.ma;
        
        })
        if(index==-1) return ;

        dsSv[index]=capNhapSv;

        randerDSSV(dsSv);
        resetForm();
        document.getElementById("txtMaSV").disabled=false;
    
}
